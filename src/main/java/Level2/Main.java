package Level2;


import io.vavr.control.Try;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) throws IOException {
        try (Scanner scanner = new Scanner(System.in);
             PrintWriter writer = new PrintWriter("/Users/gulnarahabirova/IdeaProjects/homework/src/main/resources/result2.txt")) {
            var result = FileUtils.count(scanner.nextLine());
            result.entrySet()
                    .forEach(entry -> Try.run(() -> writer.write(String.format("%s - %s\n", entry.getKey(), entry.getValue()))));
        }
    }
}
