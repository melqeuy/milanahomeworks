package Level2;


import lombok.experimental.UtilityClass;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Collections;
import java.util.Map;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

@UtilityClass
public class FileUtils {
    public static void main(String[] args) {
        var text = "й";
        Pattern pattern = Pattern.compile("[А-Яа-я\\d]");
        Matcher matcher = pattern.matcher(text);
        System.out.println(matcher.find());
    }

    public static Map<String, Long> count(String path) throws IOException {
        if (path == null || path.isBlank()) {
            return Collections.emptyMap();
        }

        Pattern pattern = Pattern.compile("[А-Яа-я\\d]");

        return Files.lines(Paths.get(path))
                 .flatMap(line -> Arrays.stream(line.split("")))
                 .filter(symbol -> pattern.matcher(symbol).find())
                 .collect(Collectors.groupingBy(Function.identity(), Collectors.counting()));
    }
}
