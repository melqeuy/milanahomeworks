package homework7;

import java.io.IOException;

public interface UsersRepositoryFile {
    User findById(int id) throws IOException;
    void create(User user);
    void update(User user);
    void delete(Integer id);
}
