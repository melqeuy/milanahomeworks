package homework7;


import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.util.Objects;
import java.util.concurrent.locks.ReentrantReadWriteLock;


public class UsersRepositoryFileImpl implements UsersRepositoryFile {
    private static final ReentrantReadWriteLock lock = new ReentrantReadWriteLock(true);
    private static final String DELIMETER = "\\|";
    private static final int ID_LINE_INDEX = 0;
    private static final int FIRST_NAME_LINE_INDEX = 1;
    private static final int SECOND_NAME_LINE_INDEX = 2;
    private static final int AGE_LINE_INDEX = 3;
    private static final int IS_WORKER_LINE_INDEX = 4;
    private static final String USER_LINE_FORMAT = "%s|%s|%s|%s|%s";
    private static final String LINE_TRANSLATION_SYMBOL = "\n";
    private static File file = new File("src/main/resources/Users.txt");
    private static File temp = new File("src/main/resources/Temp.txt");




    public User findById(int id) {
        try (var reader = new BufferedReader(new FileReader(file))) {
            var result = reader.lines()
                    .filter(line -> !line.isEmpty())
                    .filter(line -> Integer.parseInt(line.split(DELIMETER)[ID_LINE_INDEX]) == id)
                    .findFirst()
                    .map(line -> {
                        var words = line.split(DELIMETER);
                        var user = new User();
                        user.setId(Integer.parseInt(words[ID_LINE_INDEX]));
                        user.setFirstName(words[FIRST_NAME_LINE_INDEX]);
                        user.setSecondName(words[SECOND_NAME_LINE_INDEX]);
                        user.setAge(Integer.parseInt(words[AGE_LINE_INDEX]));
                        user.setWorker(Boolean.parseBoolean(words[IS_WORKER_LINE_INDEX]));
                        return user;
                    })
                    .orElseThrow(() -> new RuntimeException("Пользователь с таким id не найден " + id));
            return result;

        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }



    public void create(User user) {
        Objects.requireNonNull(user.getId(), "Не указан id для пользователя");
        try (var reader = new BufferedReader(new FileReader(file));
             var writer = new FileWriter(file, true)) {
            var contain = reader.lines()
                    .filter(line -> !line.isEmpty())
                    .map(line -> line.split(DELIMETER))
                    .map(array -> array[ID_LINE_INDEX])
                    .map(Integer::parseInt)
                    .anyMatch(id -> Objects.equals(id, user.getId()));
            if (contain) {
                throw new RuntimeException("Пользователь с таким id уже есть " + user.getId());
            }
            writer.write(generateUserLine(user));
        } catch (IOException e) {
            throw new RuntimeException(e);
        } catch (Exception e) {
            System.out.println("строка");
        }

    }



    public void update(User user) {

        Objects.requireNonNull(user.getId(), "Не указан id для пользователя");
        try (var idReader = new BufferedReader(new FileReader(file));
             var lineReader = new BufferedReader(new FileReader(file));
             var writer = new BufferedWriter(new FileWriter(temp))) {
            idReader.lines()
                    .filter(line -> !line.isEmpty())
                    .map(line -> line.split(DELIMETER))
                    .map(array -> array[ID_LINE_INDEX])
                    .map(Integer::parseInt)
                    .filter(id -> id.equals(user.getId()))
                    .findAny()
                    .orElseThrow(() -> new RuntimeException("Пользователя с таким id не существует " + user.getId()));
            lineReader.lines()
                    .filter(line -> !Objects.equals(Integer.parseInt(line.split(DELIMETER)[ID_LINE_INDEX]), user.getId()))
                    .forEach(line -> {
                        try {
                            writer.write(line + LINE_TRANSLATION_SYMBOL);
                        } catch (IOException e) {
                            throw new RuntimeException(e);
                        }
                    });
            writer.write(generateUserLine(user));

            Files.move(temp.toPath(), file.toPath(), StandardCopyOption.ATOMIC_MOVE);


        } catch (IOException e) {
            throw new RuntimeException(e);
        }

    }


    public void delete(Integer source) {
        Objects.requireNonNull(source, "Не указан id для пользователя");
        try (var idReader = new BufferedReader(new FileReader(file));
             var lineReader = new BufferedReader(new FileReader(file));
             var writer = new BufferedWriter(new FileWriter(temp))) {
            // 177 - 184 - заменить на findById()
            idReader.lines()
                    .filter(line -> !line.isEmpty())
                    .map(line -> line.split(DELIMETER))
                    .map(array -> array[ID_LINE_INDEX])
                    .map(Integer::parseInt)
                    .filter(id -> id.equals(source))
                    .findAny()
                    .orElseThrow(() -> new RuntimeException("Пользователя с таким id не существует " + source));
            lineReader.lines()
                    .filter(line -> !Objects.equals(Integer.parseInt(line.split(DELIMETER)[ID_LINE_INDEX]), source))
                    .forEach(line -> {
                        try {
                            writer.write(line + LINE_TRANSLATION_SYMBOL);
                        } catch (IOException e) {
                            throw new RuntimeException(e);
                        }
                    });

            Files.move(temp.toPath(), file.toPath(), StandardCopyOption.ATOMIC_MOVE);


        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private String generateUserLine(User source) {
       return String.format(USER_LINE_FORMAT, source.getId(), source.getFirstName(), source.getSecondName(), source.getAge(), source.isWorker());
    }
}
