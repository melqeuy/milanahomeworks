package com.example.homework;

import com.example.homework.FileUtils;
import io.vavr.control.Try;

import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) throws IOException {
        try (Scanner scanner = new Scanner(System.in);
             PrintWriter writer = new PrintWriter("/Users/gulnarahabirova/IdeaProjects/homework/src/main/resources/result.txt")) {
            var result = FileUtils.count(scanner.nextLine());
            result.entrySet()
                    .forEach(entry -> Try.run(() -> writer.write(String.format("%s - %s\n", entry.getKey(), entry.getValue()))));
        }
    }
}
