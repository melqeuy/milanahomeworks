package com.example.homework;

import lombok.experimental.UtilityClass;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Collections;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

@UtilityClass
public class FileUtils {

    public static Map<String, Long> count(String path) throws IOException {
        if (path == null || path.isBlank()) {
            return Collections.emptyMap();
        }

        return Files.lines(Paths.get(path))
                .flatMap(line -> Arrays.stream(line.split(" ")))
                .collect(Collectors.groupingBy(Function.identity(), Collectors.counting()));
    }
}
