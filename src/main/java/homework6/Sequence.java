package homework6;

import java.util.Arrays;

public class Sequence {
    public static int[] filter(final int[] array, final ByCondition condition) {
        return Arrays.stream(array)
                .filter(condition::isOk)
                .toArray();
    }

}
