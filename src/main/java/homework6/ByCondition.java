package homework6;

@FunctionalInterface
public interface ByCondition {
    boolean isOk(int number);
}
