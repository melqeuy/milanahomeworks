package homework6;

import java.util.Arrays;
import java.util.Random;
import java.util.stream.IntStream;

public class Main {
    public static void main(String[] args) {
        int[] array = new int[20];
        Random random = new Random();

        for (int i = 0; i < array.length; i++) {
            array[i] = random.nextInt(20) + 1;
        }

        System.out.println("Проверка на четность элемента:");
        System.out.println("\nЧетные элементы:");
        Arrays.stream(Sequence.filter(array, numb -> numb % 2 == 0))
                .forEach(numb -> System.out.print(numb + " "));

        System.out.println("\nНечетные элементы:");
        Arrays.stream(Sequence.filter(array, x -> x % 2 != 0))
                .forEach(x -> System.out.print(x + " "));

        System.out.println("\nПроверка, является ли сумма цифр элемента четным числом:");
        int sum = IntStream.of(array).sum();
        if (sum % 2 == 0) {
            System.out.println(sum + " - сумма цифр элемента является четным числом");
        }
        else {
            System.out.println(sum + " - сумма цифр элемента не является четным числом");
        }

        System.out.println("\nПроверка на четность всех цифр числа:");
        Arrays.stream(Sequence.filter(array, numb -> Arrays.stream(Integer.toString(numb).split(""))
                        .mapToInt(Integer::parseInt)
                        .allMatch(i -> i % 2 == 0)))
                .forEach(numb -> System.out.print(numb + " "));






    }
}
